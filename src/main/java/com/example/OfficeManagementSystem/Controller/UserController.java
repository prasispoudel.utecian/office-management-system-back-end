package com.example.OfficeManagementSystem.Controller;

import com.example.OfficeManagementSystem.Converter.EmployeeConverter;
import com.example.OfficeManagementSystem.DTO.EmployeeDTO;
import com.example.OfficeManagementSystem.Model.Employee;
import com.example.OfficeManagementSystem.Service.EmailService;
import com.example.OfficeManagementSystem.Service.EmployeeDetailService;
import com.example.OfficeManagementSystem.Service.ReportGenerationService;
import com.example.OfficeManagementSystem.Utility.JwtUtil;
import net.sf.jasperreports.engine.JRException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.web.bind.annotation.*;

import java.io.FileNotFoundException;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class UserController {
    @Autowired
    private JwtUtil jwtUtil;
    @Autowired
    private AuthenticationManager authenticationManager;
    @Autowired
    private EmployeeConverter employeeConverter;
    @Autowired
    private EmployeeDetailService employeeDetailService;
    @Autowired
    private EmailService emailService;

    @Autowired
    private ReportGenerationService reportGenerationService;
    private EmployeeDTO loginDTO;
    /*
    @GetMapping("/getUser/{id}")
    public EmployeeDTO getEmployee(@PathVariable(value = "id") Integer id){
        return employeeConverter.entitytoDTO( employeeDetailService.getEmployeeByID(id));
    }
     */

    // Controller for generation of report.
    /*
    @GetMapping("/EmployeeReport/{format}")
    public String generateEmployeeReport(@PathVariable(value = "format") String format) throws FileNotFoundException, JRException {
       return this.reportGenerationService.exportReport(format);
    }
     */

    @GetMapping("/EmployeeReport/{format}/filename/{filename}")
    public String generateEmployeeReport(@PathVariable(value = "format") String format,@PathVariable(value = "filename") String filename) throws FileNotFoundException, JRException {
        return this.reportGenerationService.exportReport(format,filename);
    }

    /*
    * Controller for fetching employee details
    */

    @GetMapping("/findAllEmployees")
    public List<EmployeeDTO> getAllEmployees(){
      try{
         List<EmployeeDTO> employeeDTO = this.employeeConverter.DTOtoEntity(this.employeeDetailService.getAllEmployees());
         return employeeDTO;
      } catch (Exception e){
          e.printStackTrace();
          return new List<EmployeeDTO>() {
              @Override
              public int size() {
                  return 0;
              }

              @Override
              public boolean isEmpty() {
                  return false;
              }

              @Override
              public boolean contains(Object o) {
                  return false;
              }

              @Override
              public Iterator<EmployeeDTO> iterator() {
                  return null;
              }

              @Override
              public Object[] toArray() {
                  return new Object[0];
              }

              @Override
              public <T> T[] toArray(T[] a) {
                  return null;
              }

              @Override
              public boolean add(EmployeeDTO employeeDTO) {
                  return false;
              }

              @Override
              public boolean remove(Object o) {
                  return false;
              }

              @Override
              public boolean containsAll(Collection<?> c) {
                  return false;
              }

              @Override
              public boolean addAll(Collection<? extends EmployeeDTO> c) {
                  return false;
              }

              @Override
              public boolean addAll(int index, Collection<? extends EmployeeDTO> c) {
                  return false;
              }

              @Override
              public boolean removeAll(Collection<?> c) {
                  return false;
              }

              @Override
              public boolean retainAll(Collection<?> c) {
                  return false;
              }

              @Override
              public void clear() {

              }

              @Override
              public EmployeeDTO get(int index) {
                  return null;
              }

              @Override
              public EmployeeDTO set(int index, EmployeeDTO element) {
                  return null;
              }

              @Override
              public void add(int index, EmployeeDTO element) {

              }

              @Override
              public EmployeeDTO remove(int index) {
                  return null;
              }

              @Override
              public int indexOf(Object o) {
                  return 0;
              }

              @Override
              public int lastIndexOf(Object o) {
                  return 0;
              }

              @Override
              public ListIterator<EmployeeDTO> listIterator() {
                  return null;
              }

              @Override
              public ListIterator<EmployeeDTO> listIterator(int index) {
                  return null;
              }

              @Override
              public List<EmployeeDTO> subList(int fromIndex, int toIndex) {
                  return null;
              }
          };
      }
    }


}
