package com.example.OfficeManagementSystem.Controller;

import com.example.OfficeManagementSystem.Converter.EmployeeConverter;
import com.example.OfficeManagementSystem.DTO.EmployeeDTO;
import com.example.OfficeManagementSystem.Model.ImageModel;
import com.example.OfficeManagementSystem.Repository.IImageRepository;
import com.example.OfficeManagementSystem.Service.EmailService;
import com.example.OfficeManagementSystem.Service.EmployeeDetailService;
import com.example.OfficeManagementSystem.Service.UserDetailService;
import com.example.OfficeManagementSystem.Utility.JwtUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.http.ResponseEntity.BodyBuilder;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.URI;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.util.function.Consumer;
import java.util.zip.DataFormatException;
import java.util.zip.Deflater;
import java.util.zip.Inflater;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class EmployeeController {
    @Autowired
    private JwtUtil jwtUtil;
    @Autowired
    private AuthenticationManager authenticationManager;
    @Autowired
    private EmployeeConverter employeeConverter;
    @Autowired
    private EmployeeDetailService employeeDetailService;
    @Autowired
    private EmailService emailService;
    @Autowired
    private UserDetailService userDetailService;
    private EmployeeDTO loginDTO;
    @Autowired
    private IImageRepository iImageRepository;
    /*
    @PostMapping("/upload")
    public BodyBuilder uplaodImage(@RequestParam("imageFile") MultipartFile file) throws IOException {


        System.out.println("Original Image Byte Size - " + file.getBytes().length);
        ImageModel img = new ImageModel(file.getOriginalFilename(), file.getContentType(),
                compressBytes(file.getBytes()));
        imageRepository.save(img);
        return ResponseEntity.status(HttpStatus.OK);

        System.out.println("Original Size of the image file: " + file.getBytes().length);
        ImageModel image = new ImageModel(file.getOriginalFilename(), file.getContentType(), compressBtye(file.getBytes()));
        iImageRepository.save(image);
        return ResponseEntity.status(HttpStatus.OK);
    }

    //  method for compressing the image file before storing in the database.
    public static byte[] compressBtye(byte [] data){
        Deflater deflater = new Deflater();
        deflater.setInput(data);
        deflater.finish();

        ByteArrayOutputStream stream = new ByteArrayOutputStream(data.length);
        byte [] buffer = new byte[1024];
        while (!deflater.finished()){
            int count = deflater.deflate(buffer);
            stream.write(buffer,0,count);
        }
        try {
            stream.close();
        }catch (Exception e){
            e.printStackTrace();
        }
        System.out.println("Compressed Image Byte Size:- "+ stream.toByteArray().length);
        return stream.toByteArray();
    }


    //method to uncompress the image file before send back to the front end as the response to the fetch request.
    public static byte[] decompressBytes(byte [] data){
        Inflater inflater= new Inflater();
        inflater.setInput(data);
        ByteArrayOutputStream outputStream  = new ByteArrayOutputStream(data.length);
        byte [] buffer = new byte[1024];
        try{
            while (!inflater.finished()) {
                int count = inflater.inflate(buffer);
                outputStream.write(buffer, 0, count);
            }
            outputStream.close();
        }catch (IOException e){
            e.printStackTrace();
        }catch (DataFormatException e){
            e.printStackTrace();
        }
        return outputStream.toByteArray();
    }



*/

}
