package com.example.OfficeManagementSystem.Controller;

import com.example.OfficeManagementSystem.Converter.EmployeeConverter;
import com.example.OfficeManagementSystem.Converter.LeaveConverter;
import com.example.OfficeManagementSystem.DTO.EmployeeDTO;
import com.example.OfficeManagementSystem.DTO.LeaveDTO;
import com.example.OfficeManagementSystem.DTO.LoginDTO;
import com.example.OfficeManagementSystem.Model.AuthRequest;
import com.example.OfficeManagementSystem.Model.Employee;
import com.example.OfficeManagementSystem.Model.ImageModel;
import com.example.OfficeManagementSystem.Model.User;
import com.example.OfficeManagementSystem.Repository.IImageRepository;
import com.example.OfficeManagementSystem.Service.*;
import com.example.OfficeManagementSystem.Utility.JwtUtil;
import net.sf.jasperreports.engine.JRException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.parameters.P;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.criteria.Predicate;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.zip.DataFormatException;
import java.util.zip.Deflater;
import java.util.zip.Inflater;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
// @CrossOrigin(origins = "*")
 // @RequestMapping("api/OfficeManagementApplication")
public class LoginController {
    @Autowired
    private JwtUtil jwtUtil;
    @Autowired
    private AuthenticationManager authenticationManager;
    @Autowired
    private EmployeeConverter employeeConverter;
    @Autowired
    private EmployeeDetailService employeeDetailService;
    @Autowired
    private EmailService emailService;
    @Autowired
    private LeaveDetailService leaveDetailService;
    @Autowired
    private LeaveConverter leaveConverter;
    @Autowired
    private UserDetailService userDetailService;
    private EmployeeDTO loginDTO;
    @Autowired
    private IImageRepository iImageRepository;
    @GetMapping("/welcome")
    public String LodgedIn(){
        return "Your have been Logged in the application";
    }
    @GetMapping("/login")
    public Boolean Login(){
       return true;
   }

    /*
    @PostMapping("register")
    public HttpStatus Register(@RequestBody Employee employee){
//     Employee employee = employeeConverter.DTOtoEntity(employeeDTO);
//     System.out.println(employee.getEmail());
        //  employeeDetailService.saveEmployee(employeeConverter.DTOtoEntity(employeeDTO));
        try {
           // employeeConverter.entitytoDTO(employeeDetailService.saveEmployee(employee));
            employeeDetailService.saveEmployee(employee);
            return HttpStatus.OK;
        }catch (NullPointerException e){
            e.printStackTrace();
        }
        return HttpStatus.CONFLICT;

   }
     */
    /* Leave Request API Controller endpoint*/
    @PostMapping("leaveRequest")
    public HttpStatus Register(@RequestBody LeaveDTO leaveDTO){
        try{
             leaveDetailService.saveEmployeeLeave(leaveConverter.DTOtoEntity(leaveDTO));
             leaveDetailService.sendLeaveRequestConfirmationEmail(leaveConverter.DTOtoEntity(leaveDTO));
             return HttpStatus.OK;
        }catch (Exception e){
            e.printStackTrace();
            return HttpStatus.NOT_IMPLEMENTED;
        }finally {
           return HttpStatus.ACCEPTED;
        }
    }

    @PostMapping("add")
    public HttpStatus Register(@RequestBody Employee employee){
//     Employee employee = employeeConverter.DTOtoEntity(employeeDTO);
//     System.out.println(employee.getEmail());
        //  employeeDetailService.saveEmployee(employeeConverter.DTOtoEntity(employeeDTO));
        try {
            // employeeConverter.entitytoDTO(employeeDetailService.saveEmployee(employee));
            employeeDetailService.saveEmployee(employee);
            return HttpStatus.OK;
        }catch (NullPointerException e){
            e.printStackTrace();
        }
        return HttpStatus.CONFLICT;
    }



    @PostMapping("post")
    public HttpStatus Post(@RequestBody EmployeeDTO employeeDTO){
       try{
           Employee employee = employeeConverter.DTOtoEntity(employeeDTO);
           System.out.println(employee.getEmail());
       //    System.out.println(employee.getUsername());
       //    System.out.println(employee.getPassword());
           EmployeeDTO employeeDTO1 = employeeConverter.entitytoDTO(employeeDetailService.saveEmployee(employeeConverter.DTOtoEntity(employeeDTO)));
        //   System.out.println(employeeDTO1.getName());
           return HttpStatus.OK;
       }catch(Exception e){
           e.printStackTrace();
           return HttpStatus.NOT_IMPLEMENTED;
       }
    }
    @GetMapping("/getUser/{id}")
    public EmployeeDTO getEmployee(@PathVariable(value = "id") Integer id){
        return employeeConverter.entitytoDTO( employeeDetailService.getEmployeeByID(id));
    }

    @GetMapping("/find/{username}")
    public User getEmployee(@PathVariable(value = "username") String username) throws Exception {
        return userDetailService.findUser(username);
    }

    /*
    @GetMapping("/find/{username}")
    public Employee getEmployee(@PathVariable(value = "username") String username) throws Exception{
        return userDetailService.findUser(username);
    }
     */


   @PostMapping("/register")
   @ResponseBody
   public HttpStatus Register(@RequestBody EmployeeDTO employeeDTO){
//     Employee employee = employeeConverter.DTOtoEntity(employeeDTO);
//     System.out.println(employee.getEmail());
     //  employeeDetailService.saveEmployee(employeeConverter.DTOtoEntity(employeeDTO));
       try {
         //  employeeConverter.entitytoDTO(employeeDetailService.saveEmployee(employeeConverter.DTOtoEntity(employeeDTO)));
            Employee employee = employeeConverter.DtotoEntity(employeeDTO);
            employeeDetailService.saveEmployee(employee);
            System.out.println(employee.getEmail());

            try {
                // storing the temporary username and password of the new employee within the database for verification and login purposes.

                //userDetailService.
                User user = new User(employee.getUsernameTemp(),employee.getPasswordTemp(),"no");
                userDetailService.setCredentials(user);
                emailService.emailSender(employee);
              //  System.out.println(employee.getDepartmentID());
            } catch (Exception e){
                e.printStackTrace();
                System.out.println("Email not sent to the new employee, please pass the credentials manually.");
            }
           return HttpStatus.OK;
       }catch (NullPointerException e){
           e.printStackTrace();
       }
       System.out.println("Not OK");
       return HttpStatus.CONFLICT;
       // employeeConverter.entitytoDTO(employeeConverter.DTOtoEntity(employeeDTO));
       /*
       if(emailService.emailSender(employee) && employeeDetailService.saveEmployee(employee)){
          return HttpStatus.CREATED;
       }
       return HttpStatus.NOT_IMPLEMENTED;
      */
   }
  /*
   @PostMapping("/getNotice")
   @ResponseBody
   public HttpStatus publishNotice(@RequestBody String content) throws Exception{
       System.out.println(content);
       return HttpStatus.OK;
   }
   */
   @PostMapping("/sendNotice")
   @ResponseBody
   public HttpStatus getNotice(@RequestBody String content) throws Exception{
       System.out.println(content);
       try {
          emailService.sendNotice(content);
          return HttpStatus.OK;
       } catch (Exception e) {
           e.printStackTrace();
       }
       return HttpStatus.NOT_IMPLEMENTED;
   }

   @PostMapping("/authenticate")
   @ResponseBody
    public String generateToken(@RequestBody AuthRequest authRequest) throws  Exception{
       try{
           authenticationManager.authenticate(
                   new UsernamePasswordAuthenticationToken(authRequest.getUsername(),authRequest.getPassword())
           );
       }catch(Exception e){
           throw new Exception("invalid username/password");
       }
       return jwtUtil.generateToken(authRequest.getUsername());
   }

    @PostMapping("/upload")
    @ResponseBody
    public ResponseEntity.BodyBuilder uplaodImage(@RequestParam("imageFile") MultipartFile file) throws IOException {

        /*
        System.out.println("Original Image Byte Size - " + file.getBytes().length);
        ImageModel img = new ImageModel(file.getOriginalFilename(), file.getContentType(),
                compressBytes(file.getBytes()));
        imageRepository.save(img);
        return ResponseEntity.status(HttpStatus.OK);
         */
        System.out.println("Original Size of the image file: " + file.getBytes().length);
        ImageModel image = new ImageModel(file.getOriginalFilename(), file.getContentType(), compressBtye(file.getBytes()));
        iImageRepository.save(image);
        return ResponseEntity.status(HttpStatus.OK);
    }

    //  method for compressing the image file before storing in the database.
    public static byte[] compressBtye(byte [] data){
        Deflater deflater = new Deflater();
        deflater.setInput(data);
        deflater.finish();

        ByteArrayOutputStream stream = new ByteArrayOutputStream(data.length);
        byte [] buffer = new byte[1024];
        while (!deflater.finished()){
            int count = deflater.deflate(buffer);
            stream.write(buffer,0,count);
        }
        try {
            stream.close();
        }catch (Exception e){
            e.printStackTrace();
        }
        System.out.println("Compressed Image Byte Size:- "+ stream.toByteArray().length);
        return stream.toByteArray();
    }


    //method to uncompress the image file before send back to the front end as the response to the fetch request.
    public static byte[] decompressBytes(byte [] data){
        Inflater inflater= new Inflater();
        inflater.setInput(data);
        ByteArrayOutputStream outputStream  = new ByteArrayOutputStream(data.length);
        byte [] buffer = new byte[1024];
        try{
            while (!inflater.finished()) {
                int count = inflater.inflate(buffer);
                outputStream.write(buffer, 0, count);
            }
            outputStream.close();
        }catch (IOException e){
            e.printStackTrace();
        }catch (DataFormatException e){
            e.printStackTrace();
        }
        return outputStream.toByteArray();
    }
}
