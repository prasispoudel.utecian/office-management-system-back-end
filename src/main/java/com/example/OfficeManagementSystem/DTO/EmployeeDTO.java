package com.example.OfficeManagementSystem.DTO;

import com.example.OfficeManagementSystem.Model.Department;
import com.example.OfficeManagementSystem.Model.User;

public class EmployeeDTO {

    private String name;
    private String number;
    private String Position;
    private String email;
    private String usernameTemp;
    private String passwordTemp;
    //private User User;
    private Department Department;
   // private byte[] picByte;
/*
    public EmployeeDTO(String originalFilename, String contentType, byte[] compressBtye) {
        this.picByte =  compressBtye;
    }
 */

    public EmployeeDTO(){

    }
    public EmployeeDTO(String name, String number, String position, String email, String usernameTemp, String passwordTemp, com.example.OfficeManagementSystem.Model.Department department, byte[] picByte) {
        this.name = name;
        this.number = number;
        Position = position;
        this.email = email;
        this.usernameTemp = usernameTemp;
        this.passwordTemp = passwordTemp;
        Department = department;
       // this.picByte = picByte;
    }

    public String getUsernameTemp() {
        return usernameTemp;
    }

    public void setUsernameTemp(String usernameTemp) {
        this.usernameTemp = usernameTemp;
    }

    public String getPasswordTemp() {
        return passwordTemp;
    }

    public void setPasswordTemp(String passwordTemp) {
        this.passwordTemp = passwordTemp;
    }
   /*
    public com.example.OfficeManagementSystem.Model.User getUser() {
        return User;
    }

    public void setUser(com.example.OfficeManagementSystem.Model.User user) {
        User = user;
    }
    */
/*
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }
    /*
    public Department getDepartment() {
        return Department;
    }

    public void setDepartment(Department department) {
        Department = department;
    }
*/
    /*
    public String getPosition() {
        return Position;
    }

    public void setPosition(String position) {
        Position = position;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
     */

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getPosition() {
        return Position;
    }

    public void setPosition(String position) {
        Position = position;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
