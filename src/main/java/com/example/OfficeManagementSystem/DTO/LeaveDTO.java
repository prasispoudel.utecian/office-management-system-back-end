package com.example.OfficeManagementSystem.DTO;

import com.example.OfficeManagementSystem.Model.User;

public class LeaveDTO {
    private String startdate;
    private String enddate;
    private String Resaon;
    private User userid;


    public LeaveDTO() {
    }

    public LeaveDTO(String startdate, String enddate, String resaon, User userid) {
        this.startdate = startdate;
        this.enddate = enddate;
        Resaon = resaon;
        this.userid = userid;
    }

    public String getStartdate() {
        return startdate;
    }

    public void setStartdate(String startdate) {
        this.startdate = startdate;
    }

    public String getEnddate() {
        return enddate;
    }

    public void setEnddate(String enddate) {
        this.enddate = enddate;
    }

    public String getResaon() {
        return Resaon;
    }

    public void setResaon(String resaon) {
        Resaon = resaon;
    }

    public User getUserid() {
        return userid;
    }

    public void setUserid(User userid) {
        this.userid = userid;
    }

    @Override
    public String toString() {
        return "LeaveDTO{" +
                "startdate='" + startdate + '\'' +
                ", enddate='" + enddate + '\'' +
                ", Resaon='" + Resaon + '\'' +
                ", userid=" + userid +
                '}';
    }
}
