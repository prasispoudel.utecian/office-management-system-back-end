package com.example.OfficeManagementSystem.Service;
import com.example.OfficeManagementSystem.Model.Employee;
import com.example.OfficeManagementSystem.Repository.IEmployeeRepository;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ReportGenerationService {
@Autowired
private IEmployeeRepository iEmployeeRepository;
public String exportReport(String reportFormat,String filename) throws FileNotFoundException, JRException {
    String path = "C:\\Users\\my986\\Documents\\JasperReports";
    List<Employee> employees = this.iEmployeeRepository.findAll();
    File file = ResourceUtils.getFile("classpath:employees.jrxml");
    JasperReport jasperReport = JasperCompileManager.compileReport(file.getAbsolutePath());
    JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(employees);
    Map<String,Object> parameters = new HashMap<>();
    parameters.put("Created By:- ", "Prasis Poudel");
    JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport,parameters,dataSource);
    // checking for the report format before report generation(pdf or Html)
    if(reportFormat.equalsIgnoreCase("pdf") ){
           JasperExportManager.exportReportToPdfFile(jasperPrint, path+"\\"+filename+".pdf");
    }
    if( reportFormat.equalsIgnoreCase("html")){
          JasperExportManager.exportReportToHtmlFile(jasperPrint, path+"\\"+filename+".html");
    }
    return "report generated in path:- "+path+ "with filename:- "+filename;
}
}
