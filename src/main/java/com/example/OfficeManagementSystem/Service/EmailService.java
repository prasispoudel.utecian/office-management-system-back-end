package com.example.OfficeManagementSystem.Service;
import com.example.OfficeManagementSystem.Model.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import javax.mail.internet.MimeMessage;

@Service
public class EmailService {
    private JavaMailSender javaMailSender;
    @Autowired
    public EmailService(JavaMailSender javaMailSender) {
        this.javaMailSender = javaMailSender;
    }

    public boolean emailSender(Employee employee) throws MailException {
        try{
            /* Getting the credentials from the  user table.
             get the initial username and password from the user table for sending it the the new employee through email.
             Sending Email to the new employee with temporary username and password.*/
            /*--------Mail with message within HTML and Logo Image-----*/
            MimeMessage simpleMailMessage = javaMailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(simpleMailMessage,true);
            helper.setFrom("UTechITProfessions@gmail.com","UTechItProfessionals");
            String mailContent = "<p><b>Your New Credentials for the management system are:-</p>";
            mailContent += "<p><b>Username:-</p> "+ employee.getUsernameTemp();
            mailContent += "<p><b>Password:-</p> "+ employee.getPasswordTemp();
            mailContent += "<p>Thank you</p>";
            mailContent += "<hr><img src='cid:LogoImage'/>";
            mailContent += "";
            helper.setTo("hirakauchha1234@gmail.com");
            helper.setSubject("Credentials for login");
            // helper.setSubject("Congratulations! you have been approved as new employee of UTechIT Professionals.");
            helper.setText(mailContent,true);
            ClassPathResource classPathResource = new ClassPathResource("static/Images/UTechlogo.png");
            helper.addInline("LogoImage",classPathResource);
            javaMailSender.send(simpleMailMessage);
            /*----------------------------------------------------------------------------------------*/
            /*------Simple Mail With text Only------*/
            /*
            SimpleMailMessage simpleMailMessage = new SimpleMailMessage();
            //simpleMailMessage.setTo(employee.getEmail());
            simpleMailMessage.setTo("prasis0103@gmail.com"); // for testing purposes only
            simpleMailMessage.setFrom("HR@utechit.com");
            simpleMailMessage.setSubject("Congratulations! you have been approved as new employee of UTechIT Professionals.");
            simpleMailMessage.setText("Your New Credentials for the management system are:- \n" +
                    "Username:- "+ employee.getUsernameTemp() +"\n"+
                    "Password:- " + employee.getPasswordTemp() +"\n"+
                    "Please immediately change your default password by logging within the Management Application.\n"+
                    "Thank you");
            javaMailSender.send(simpleMailMessage);
             */
            return true;
        }catch(Exception e){
            e.printStackTrace();
            return false;
        }
    }

    public boolean sendNotice(String content) throws Exception{
        try{
            MimeMessage mimeMessage = javaMailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(mimeMessage,true);
            helper.setFrom("UTechITProfessions@gmail.com","System Admin");
            String mailContent = content;
            mailContent += "<hr><img src='cid:LogoImage'/>";
            helper.setTo("prasis0103@gmail.com");
            // sakarpradhansp@gmail.com
            helper.setSubject("Notice from Administrator");
            helper.setText(mailContent,true);
            ClassPathResource classPathResource = new ClassPathResource("static/Images/UTechlogo.png");
            helper.addInline("LogoImage",classPathResource);
            javaMailSender.send(mimeMessage);
            return true;
        }catch (Exception e){
            e.printStackTrace();
        }
        return false;
    }
}
