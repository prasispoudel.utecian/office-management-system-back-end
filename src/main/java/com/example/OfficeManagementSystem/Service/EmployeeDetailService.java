package com.example.OfficeManagementSystem.Service;

import com.example.OfficeManagementSystem.Model.Employee;
import com.example.OfficeManagementSystem.Repository.IEmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class EmployeeDetailService {
    @Autowired
    private IEmployeeRepository iEmployeeRepository;
    public Employee saveEmployee(Employee employee){
        return iEmployeeRepository.save(employee);
    }
    public Employee getEmployeeByID(Integer id){
        return iEmployeeRepository.getEmployeeById(id);
    }
    public List<Employee> getAllEmployees(){
        return iEmployeeRepository.findAll();
    }
}
