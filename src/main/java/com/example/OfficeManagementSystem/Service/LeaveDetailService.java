package com.example.OfficeManagementSystem.Service;

import com.example.OfficeManagementSystem.Model.Employee;
import com.example.OfficeManagementSystem.Model.EmployeeLeave;
import com.example.OfficeManagementSystem.Repository.IEmployeeRepository;
import com.example.OfficeManagementSystem.Repository.ILeaveRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.UnsupportedEncodingException;

@Service
public class LeaveDetailService {
    @Autowired
    private ILeaveRepository iLeaveRepository;
    @Autowired
    private IEmployeeRepository employeeRepository;
    private JavaMailSender javaMailSender;

    public LeaveDetailService(JavaMailSender javaMailSender) {
        this.javaMailSender = javaMailSender;
    }

    public void saveEmployeeLeave(EmployeeLeave employeeLeave){
       this.iLeaveRepository.save(employeeLeave);
   }
   public void sendLeaveRequestConfirmationEmail(EmployeeLeave employeeLeave) throws MessagingException, UnsupportedEncodingException {
       // fetching email address of user from user table with the corresponding user id mentioned within the EmployeeLeave table.
       MimeMessage simpleMailMessage = javaMailSender.createMimeMessage();
       MimeMessageHelper helper = new MimeMessageHelper(simpleMailMessage,true);
       helper.setFrom("UTechITProfessions@gmail.com","UTechItProfessionals");
       String mailContent = "<p><b>Your request for leave with conditions:-</p>";
       mailContent += "<p><b>Start Date:-</p> "+ employeeLeave.getStartdate();
       mailContent += "<p><b>End date:-</p> "+ employeeLeave.getEnddate();
       mailContent += "<p><b>Your Reasoning:-</p> " + employeeLeave.getResaon();
       mailContent += "<p>Has been registered within the system,Thank you!</p>";
       mailContent += "<hr><img src='cid:LogoImage'/>";
       mailContent += "";
       helper.setSubject(this.employeeRepository.getEmployeeById(employeeLeave.getUserid().getUserid()).getEmail());
       // helper.setTo("hirakauchha1234@gmail.com");
       helper.setSubject("Credentials for login");
       // helper.setSubject("Congratulations! you have been approved as new employee of UTechIT Professionals.");
       helper.setText(mailContent,true);
       ClassPathResource classPathResource = new ClassPathResource("static/Images/UTechlogo.png");
       helper.addInline("LogoImage",classPathResource);
       javaMailSender.send(simpleMailMessage);

   }
}
