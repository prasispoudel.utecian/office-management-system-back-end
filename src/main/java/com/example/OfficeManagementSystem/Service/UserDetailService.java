package com.example.OfficeManagementSystem.Service;

import com.example.OfficeManagementSystem.Model.Employee;
import com.example.OfficeManagementSystem.Model.User;
import com.example.OfficeManagementSystem.Repository.IEmployeeRepository;
import com.example.OfficeManagementSystem.Repository.IFileDBRepository;
import com.example.OfficeManagementSystem.Repository.IUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.annotation.Id;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.stream.Stream;

@Service
public class UserDetailService implements UserDetailsService {
    @Autowired
    private IUserRepository iUserRepository;
    @Autowired
    private IEmployeeRepository iEmployeeRepository;
    @Autowired
    private IFileDBRepository iFileDBRepository;
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = iUserRepository.getUserByUsername(username);
        System.out.println("From: -" + user.getIsAdmin());
        return new org.springframework.security.core.userdetails.User(user.getUsername(),user.getPassword(), new ArrayList<>());
    }

    public User findUser(String username) throws Exception{
        User user = iUserRepository.getUserByUsername(username);
        return user;
    }
    public User store(MultipartFile file) throws IOException {
        String filename = StringUtils.cleanPath(file.getOriginalFilename());
        User user = new User(filename,file.getContentType(),file.getBytes());
        return  iFileDBRepository.save(user);
    }

    public User getFile(String id){
       return iFileDBRepository.findById(id).get();
    }
    public Stream<User> getAllFiles(){
        return iFileDBRepository.findAll().stream();
    }
    public void setCredentials(User user){
        iUserRepository.save(user);
    }
    /*
    public Employee findUser(String username) throws Exception{
        Employee employee = iEmployeeRepository.getEmployeeByUsername(username);
        return employee;
    }
     */
}
