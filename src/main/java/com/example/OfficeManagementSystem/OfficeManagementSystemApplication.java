package com.example.OfficeManagementSystem;

import com.example.OfficeManagementSystem.Repository.IUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@SpringBootApplication
public class OfficeManagementSystemApplication {
    /*
	@Autowired
	private IUserRepository iUserRepository;
     */
   /*
	@PostConstruct
	public void initUsers(){
		List<User> users  = Stream.of(
				new User(101,"admin","admin","prasis0103@gmail.com")
		).collect(Collectors.toList());
		iEmployeeRepository.saveAll(users);
	}
    */

	@Bean
	public WebMvcConfigurer corsConfigurer(){
		return new WebMvcConfigurer() {
			@Override
			public void addCorsMappings(CorsRegistry registry) {
				registry.addMapping("/*").allowedHeaders("*").allowedOrigins("http://localhost:4200").allowedMethods("*").allowCredentials(true);
			}
		};
	}



	public static void main(String[] args) {
		SpringApplication.run(OfficeManagementSystemApplication.class, args);
	}

}
