package com.example.OfficeManagementSystem.Model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="EmployeeLeave")
public class EmployeeLeave {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="leaveid",unique = true)
    private int leaveid;
    @Column(name="startdate")
    private String startdate;
    @Column(name="enddate")
    private String enddate;
    @Column(name = "Reason")
    private String Resaon;
    @OneToOne(targetEntity = User.class)
    private User userid;


    public int getLeaveid() {
        return leaveid;
    }

    public void setLeaveid(int leaveid) {
        this.leaveid = leaveid;
    }

    public String getStartdate() {
        return startdate;
    }

    public void setStartdate(String startdate) {
        this.startdate = startdate;
    }

    public String getEnddate() {
        return enddate;
    }

    public void setEnddate(String enddate) {
        this.enddate = enddate;
    }

    public String getResaon() {
        return Resaon;
    }

    public void setResaon(String resaon) {
        Resaon = resaon;
    }

    public User getUserid() {
        return userid;
    }

    public void setUserid(User userid) {
        this.userid = userid;
    }

    @Override
    public String toString() {
        return "EmployeeLeave{" +
                "leaveid=" + leaveid +
                ", startdate='" + startdate + '\'' +
                ", enddate='" + enddate + '\'' +
                ", Resaon='" + Resaon + '\'' +
                ", userid=" + userid +
                '}';
    }
}
