package com.example.OfficeManagementSystem.Model;

import javax.persistence.*;

@Entity
@Table(name="Employee_Details")
public class Employee {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="Empid",unique = true)
    private int id;
    @Column(name="name")
    private String name;
    @Column(name = "number")
    private String number;
    @Column(name = "position")
    private String position;
    @Column(name = "email")
    private String email;
//    @Lob
 //   private byte[] data;
    @Column(name = "usernameTemp")
    private String usernameTemp;
    @Column(name="passwordTemp")
    private String passwordTemp;

    @Column(name = "picByte", length = 1000)
    private byte[] picByte;
 //   @ManyToOne
 //   private Department department;
    /*
    @OneToOne(targetEntity = Department.class)
    private Department DepartmentID;

   // @OneToOne(targetEntity = User.class)
     */
    @OneToOne(targetEntity = Department.class)
    private Department DepartmentID;
   // private User userid;
    public Employee() {
         super();
    }

    public Employee(byte[] picByte) {
        this.picByte = picByte;
    }

    public Employee(String name, String number, String position, String email, String usernameTemp, String passwordTemp, byte[] picByte, Department departmentID) {
        this.name = name;
        this.number = number;
        this.position = position;
        this.email = email;
        this.usernameTemp = usernameTemp;
        this.passwordTemp = passwordTemp;
        this.picByte = picByte;
        DepartmentID = departmentID;
    }

    public void setId(int id) {
        this.id = id;
    }


/*
    public User getUserid() {
        return userid;
    }

    public void setUserid(User userid) {
        this.userid = userid;
    }
*/
    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getNumber() {
        return number;
    }

    public String getPosition() {
        return position;
    }

    public String getEmail() {
        return email;
    }


/*
    public byte[] getData() {
        return data;
    }
    public void setData(byte[] data) {
        this.data = data;
    }
*/
    public String getUsernameTemp() {
        return usernameTemp;
    }

    public void setUsernameTemp(String usernameTemp) {
        this.usernameTemp = usernameTemp;
    }

    public String getPasswordTemp() {
        return passwordTemp;
    }

    public void setPasswordTemp(String passwordTemp) {
        this.passwordTemp = passwordTemp;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", number='" + number + '\'' +
                ", position='" + position + '\'' +
                ", email='" + email + '\'' +
                ", usernameTemp='" + usernameTemp + '\'' +
                ", passwordTemp='" + passwordTemp + '\'' +
                '}';
    }

    /*
    @Override
    public String toString() {
        return "Employee{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", number='" + number + '\'' +
                ", position='" + position + '\'' +
                ", email='" + email + '\'' +
                ", usernameTemp='" + usernameTemp + '\'' +
                ", passwordTemp='" + passwordTemp + '\'' +
                ", department=" + department +
                '}';
    }
     */
}
