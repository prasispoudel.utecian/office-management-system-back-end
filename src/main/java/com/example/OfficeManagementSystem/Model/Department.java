package com.example.OfficeManagementSystem.Model;

import javax.persistence.*;

@Entity
@Table(name="Department_Details")
public class Department {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="DepartmentID",unique = true)
    private Integer DepartmentID;
    @Column(name="DepartmentName")
    private String DepartmentName;
    @Column(name="DepartmentHead")
    private String DepartmentHead;

    @Override
    public String toString() {
        return "Department{" +
                "DepartmentID=" + DepartmentID +
                ", DepartmentName='" + DepartmentName + '\'' +
                ", DepartmentHead='" + DepartmentHead + '\'' +
                '}';
    }
}
