package com.example.OfficeManagementSystem.Repository;

import com.example.OfficeManagementSystem.Model.Employee;
import com.example.OfficeManagementSystem.Model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface IUserRepository extends JpaRepository<User,Integer> {
    User getUserByUsername(String userName);
}
