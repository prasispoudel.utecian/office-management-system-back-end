package com.example.OfficeManagementSystem.Repository;

import com.example.OfficeManagementSystem.Model.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface IEmployeeRepository extends JpaRepository<Employee, Integer> {
      Employee getEmployeeById(Integer id);
    //  Employee getEmployeeByUsername(String username);
     // Employee findById(Integer id);
    //  Optional<Employee> findById(Integer id);
     // Optional<Employee> getEmployeeByName(String name);
}
