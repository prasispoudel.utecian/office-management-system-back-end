package com.example.OfficeManagementSystem.Repository;

import com.example.OfficeManagementSystem.Model.Employee;
import com.example.OfficeManagementSystem.Model.EmployeeLeave;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ILeaveRepository extends JpaRepository<EmployeeLeave , Integer> {
   EmployeeLeave findAllByUserid(Integer userid);
}
