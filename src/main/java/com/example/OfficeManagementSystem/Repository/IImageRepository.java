package com.example.OfficeManagementSystem.Repository;

import com.example.OfficeManagementSystem.Model.ImageModel;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface IImageRepository extends JpaRepository<ImageModel, Integer> {
    Optional<ImageModel> findByName(String name);
}
