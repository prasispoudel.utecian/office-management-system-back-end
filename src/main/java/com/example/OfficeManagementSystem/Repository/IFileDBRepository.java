package com.example.OfficeManagementSystem.Repository;

import com.example.OfficeManagementSystem.Model.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface IFileDBRepository extends JpaRepository<User, String> {
    @Override
    Optional<User> findById(String s);
}
