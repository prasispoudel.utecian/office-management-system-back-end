package com.example.OfficeManagementSystem.Interceptor;

import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class LoginInterceptor implements HandlerInterceptor {
      @Override
       public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception{
          boolean flag = true;
          String method = request.getMethod();
          if(method.equalsIgnoreCase("post")){
              System.out.println("New POST Request Received");
              String contentType = request.getContentType();
              if(contentType != null && !contentType.equalsIgnoreCase("application/json")){
                  flag = false;
              }
          }
          if(!flag){
              System.out.println("Invalid Request");
          }
          return flag;
      }


      @Override
      public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception{
          System.out.println("Starting Authentication process...");
          System.out.println("Authenticating request...");
      }

      @Override
      public void afterCompletion(HttpServletRequest request,HttpServletResponse response, Object handler, Exception exception) throws Exception{
          System.out.println("Request authentication procedure completed.");
      }

}
