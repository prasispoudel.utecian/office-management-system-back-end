package com.example.OfficeManagementSystem.Converter;

import com.example.OfficeManagementSystem.DTO.EmployeeDTO;
import com.example.OfficeManagementSystem.DTO.LeaveDTO;
import com.example.OfficeManagementSystem.Model.Employee;
import com.example.OfficeManagementSystem.Model.EmployeeLeave;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class LeaveConverter {
    public LeaveDTO entitytoDTO(EmployeeLeave leave){
        ModelMapper mapper = new ModelMapper();
        LeaveDTO map = mapper.map(leave,LeaveDTO.class);
        return map;
    }
    public EmployeeLeave DTOtoEntity(LeaveDTO leaveDTO){
        ModelMapper mapper= new ModelMapper();
        EmployeeLeave map = mapper.map(leaveDTO,EmployeeLeave.class);
        return map;
    }
    /*
    public List<EmployeeLeave> entitytoDTO(List<LeaveDTO> list) {
        return list.stream().map(dto -> super.map(dto, EmployeeLeave.class)).collect(Collectors.toList());
    }
    public List<LeaveDTO> DTOtoEntity(List<EmployeeLeave> list) {
        return list.stream().map(entity -> super.map(entity, LeaveDTO.class)).collect(Collectors.toList());
    }
     */
}
