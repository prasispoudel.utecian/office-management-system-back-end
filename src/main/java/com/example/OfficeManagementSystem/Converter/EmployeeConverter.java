package com.example.OfficeManagementSystem.Converter;

import com.example.OfficeManagementSystem.DTO.EmployeeDTO;
import com.example.OfficeManagementSystem.Model.Employee;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;

import javax.persistence.Converter;
import java.util.List;
import java.util.stream.Collectors;
@Component
public class EmployeeConverter extends ModelMapper {
    public EmployeeDTO entitytoDTO(Employee employee){
        ModelMapper mapper = new ModelMapper();
        EmployeeDTO map = mapper.map(employee,EmployeeDTO.class);
        return map;
    }
    public Employee DTOtoEntity(EmployeeDTO employeeDTO){
        ModelMapper mapper= new ModelMapper();
        Employee map = mapper.map(employeeDTO,Employee.class);
        return map;
    }
    public Employee DtotoEntity(EmployeeDTO employeeDTO){
        ModelMapper mapper = new ModelMapper();
        Employee mappedObject = mapper.map(employeeDTO,Employee.class);
        return mappedObject;
    }
    public List<Employee> entitytoDTO(List<EmployeeDTO> list) {
        return list.stream().map(dto -> super.map(dto, Employee.class)).collect(Collectors.toList());
    }
    public List<EmployeeDTO> DTOtoEntity(List<Employee> list) {
        return list.stream().map(entity -> super.map(entity, EmployeeDTO.class)).collect(Collectors.toList());
    }
}
