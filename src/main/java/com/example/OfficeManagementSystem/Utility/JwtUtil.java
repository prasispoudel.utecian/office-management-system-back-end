package com.example.OfficeManagementSystem.Utility;

import com.example.OfficeManagementSystem.Repository.IUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.function.Function;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.security.core.userdetails.UserDetails;

@Service
public class JwtUtil {
    @Autowired
    private IUserRepository iUserRepository;
    private String role;
    private String secret = "OfficeManagementSystem";
    public String extractUsername(String token) {
        return extractClaim(token, Claims::getSubject);
    }

    public Date extractExpiration(String token) {
        return extractClaim(token, Claims::getExpiration);
    }

    public <T> T extractClaim(String token, Function<Claims, T> claimsResolver) {
        final Claims claims = extractAllClaims(token);
        return claimsResolver.apply(claims);
    }
    private Claims extractAllClaims(String token) {
        return Jwts.parser().setSigningKey(secret).parseClaimsJws(token).getBody();
    }
    private Boolean isTokenExpired(String token) {
        return extractExpiration(token).before(new Date());
    }

    public String generateToken(String username) {
        Map<String, Object> claims = new HashMap<>();
        String isAdmin = (this.iUserRepository.getUserByUsername(username)).getIsAdmin();
        String id = Integer.toString(this.iUserRepository.getUserByUsername(username).getUserid());
        System.out.println(isAdmin);
        if (isAdmin == "yes"){
            this.role = "admin";
            claims.put("role", isAdmin);
            claims.put("id", id);
        }
        else{
            this.role = "employee";
            claims.put("role",isAdmin);
            claims.put("id",id);
        }
        // return createToken(claims, username, this.role);
        return createToken(claims,username,this.role);
    }

    private String createToken(Map<String, Object> claims, String subject, String role) {

        return Jwts.builder().setClaims(claims).setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + 1000 * 60 * 60 * 10))
                .signWith(SignatureAlgorithm.HS256, secret).compact();
    }

    public Boolean validateToken(String token, UserDetails userDetails) {
        final String username = extractUsername(token);
        return (username.equals(userDetails.getUsername()) && !isTokenExpired(token));
    }
}
